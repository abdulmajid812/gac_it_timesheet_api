﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class EmployeeTimeSheet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [Required]
        public int EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }
        [Required]
        public int TaskID { get; set; }
        [ForeignKey("TaskID")]
        public virtual Task Task { get; set; }
        [Required]
        public float WorkingHours { get; set; }

        [Required]
        public DateTime WorkingDate { get; set; }
    }
}
