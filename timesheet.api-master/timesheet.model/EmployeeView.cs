﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class EmployeeView
    {
        public int EmployeeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string TotalEffort { get; set; }
        public string AverageEffort { get; set; }
    }
}
