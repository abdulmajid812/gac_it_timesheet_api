﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }
        public IQueryable<Task> GetTasks()
        {
            return this.db.Tasks;
        }
        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public IQueryable<EmployeeTimeSheet> GetTimeSheetByEmployeeID(int employeeID, DateTime startDate, DateTime endDate)
        {
            return this.db.EmployeeTimeSheets.Where(p => p.EmployeeID == employeeID && p.WorkingDate >= startDate && p.WorkingDate <= endDate);
        }

        public IQueryable<EmployeeTimeSheet> GetTimeSheetByRange(DateTime startDate, DateTime endDate)
        {
            return this.db.EmployeeTimeSheets.Where(p => p.WorkingDate >= startDate && p.WorkingDate <= endDate);
        }
        public void SaveTimeSheet(int employeeid, int taskid, string workingdate, string workinghours)
        {
            this.db.EmployeeTimeSheets.Add(new EmployeeTimeSheet()
            {
                EmployeeID = employeeid,
                TaskID = taskid,
                WorkingDate = Convert.ToDateTime(workingdate),
                WorkingHours = float.Parse(workinghours)
            });
            this.db.SaveChanges();
        }
    }
}
