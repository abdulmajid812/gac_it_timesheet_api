﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;
        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var employees = this.employeeService.GetEmployees();
            var sundayDate = DateTime.Now.AddDays(NumberOfDaysToGetSunday(DateTime.Now.DayOfWeek.ToString()));
            var reporting = this.employeeService.GetTimeSheetByRange(sundayDate, sundayDate.AddDays(6));
            var listEmployees = new List<EmployeeView>();
            foreach (var item in employees)
            {
                var employeeReporting = reporting.Where(p => p.EmployeeID == item.Id).ToList();
                var totalEffort = employeeReporting.Sum(p => p.WorkingHours);
                var avgEffort = totalEffort / 7;
                listEmployees.Add(new EmployeeView()
                {
                    EmployeeID = item.Id,
                    Code = item.Code,
                    AverageEffort = avgEffort.ToString(),
                    TotalEffort = totalEffort.ToString(),
                    Name = item.Name

                });
            }

            return new ObjectResult(listEmployees);
        }

        [HttpGet("getalltasks")]
        public IActionResult GetAllTasks(string text)
        {
            var tasks = this.employeeService.GetTasks();

            return new ObjectResult(tasks);
        }

        [HttpGet("savetimesheet")]
        public IActionResult SaveTimeSheet(int employeeid, int taskid, string workingdate,string workinghours)
        {
            this.employeeService.SaveTimeSheet(employeeid, taskid, workingdate, workinghours);

            return new ObjectResult("Success");
        }

        [HttpGet("getemployeetimesheet")]
        public IActionResult GetEmployeeTimeSheet(int employeeid, string start)
        {
            var startDate = Convert.ToDateTime(start);
            var tasks = this.employeeService.GetTasks();
            var reporting = this.employeeService.GetTimeSheetByEmployeeID(employeeid, startDate, startDate.AddDays(6));
            var reportedTaskIds = reporting.Select(p => p.TaskID).Distinct();
            var listTimeSheetView = new List<EmployeeTimeSheetView>();
            foreach (var item in reportedTaskIds)
            {
                var task = tasks.FirstOrDefault(p => p.Id == item);
                var taskSpecificReporting = reporting.Where(p => p.TaskID == item);
                
                listTimeSheetView.Add(new EmployeeTimeSheetView()
                {
                    TaskName=task.Name,
                    Sunday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate)).Sum(p=>p.WorkingHours).ToString(),
                    Monday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate.AddDays(1))).Sum(p=>p.WorkingHours).ToString(),
                    Tuesday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate.AddDays(2))).Sum(p=>p.WorkingHours).ToString(),
                    Wednesday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate.AddDays(3))).Sum(p=>p.WorkingHours).ToString(),
                    Thursday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate.AddDays(4))).Sum(p=>p.WorkingHours).ToString(),
                    Friday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate.AddDays(5))).Sum(p=>p.WorkingHours).ToString(),
                    Saturday= taskSpecificReporting.Where(p => string.Format("{0:MM/dd/yyyy}", p.WorkingDate) == string.Format("{0:MM/dd/yyyy}", startDate.AddDays(6))).Sum(p=>p.WorkingHours).ToString()
                });
            }

            return new ObjectResult(listTimeSheetView);
        }

        public int NumberOfDaysToGetSunday(string dayOfWeek)
        {
            int noDays = 0;
            switch (dayOfWeek.ToLower())
            {
                case "monday":
                    noDays = -1;
                    break;
                case "tuesday":
                    noDays = -2;
                    break;
                case "wednesday":
                    noDays = -3;
                    break;
                case "thursday":
                    noDays = -4;
                    break;
                case "friday":
                    noDays = -5;
                    break;
                case "saturday":
                    noDays = -6;
                    break;
                default:
                    noDays = 0;
                    break;
            }
            return noDays;

        }
    }
}